#!/usr/bin/python3
from __future__ import print_function
import sys
import time
import logging
import rtc3d

logging.basicConfig(level=logging.INFO)
host = ""   # default host (empty string is localhost)
port = 3375 # default port (3375 for sled, 3020 for first principles)
try:
	host = sys.argv[1]
	port = int(sys.argv[2])
except IndexError:
	pass
print("connecting to {}:{:d}".format(host, port))
client = rtc3d.AsyncClient(host, port)
client.handle3D = lambda self, p: print("p: ", p)
client.startStream()
t0 = time.time()
while time.time() < t0 + 0.1:
	client.receive()
	client.parse()
client.stopStream()
