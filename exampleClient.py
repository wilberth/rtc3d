#!/usr/bin/python3
import logging
import rtc3d
import time

if __name__ == "__main__":
	logging.root.setLevel(logging.INFO)
	client = rtc3d.Client('localhost', 3375)
	t0 = time.time()
	for i in range(5):
		print("{:3.3f}: {:.3f}".format(time.time()-t0, client.get3D()[0,0]))
		time.sleep(0.35)
	for i in range(3):
		print("{:3.3f}: {:.3f}".format(time.time()-t0, client.wait3D(timeout=3.0)[0,0]))
