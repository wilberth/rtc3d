#!/usr/bin/python3

import socket, sys, struct, time, numpy as np
import PyQt4.QtGui as QtGui
import rtc3d


class Window(QtGui.QWidget):
	def __init__(self, parent = None):
		super(Window, self).__init__(parent)
		self.client = rtc3d.QClient(self, "", 3375)
		# widgets
		self.spinBox = QtGui.QSpinBox()
		self.spinBox.setMaximum(10000)
		self.spinBox.setValue(100)
		self.stopButton = QtGui.QPushButton(self.tr("Sto&p"))
		self.startButton = QtGui.QPushButton(self.tr("&Start"))
		#layout
		layout = QtGui.QGridLayout()
		layout.addWidget(self.stopButton, 0, 0)
		layout.addWidget(self.spinBox, 0, 1)
		layout.addWidget(self.startButton, 0, 2)
		self.setLayout(layout)
		
		self.client.the3D.connect(lambda x:self.spinBox.setValue(int(100*x[0,0])))
		self.stopButton.clicked.connect(self.client.pause)
		self.startButton.clicked.connect(self.client.unPause)
		

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	window = Window()
	window.show()
	sys.exit(app.exec_())