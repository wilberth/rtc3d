#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""API for the RTC3D communication protocol which is used in NDI First Principles
and DCC Sensorimotorlab sled."""

from __future__ import print_function
import socket
import sys
import struct
import time
import threading
import logging
import copy
import numpy as np

class AsyncClient(object):
	"""Asynchronous client for NDI First Principles"""
	## package and component types
	pTypes = ['Error', 'Command', 'XML', 'Data', 'Nodata', 'C3D']
	cTypes = ['', '3D', 'Analog', 'Force', '6D', 'Event']

	def __init__(self, host='', port=3020):
		if sys.platform == "win32":
			# on windows time.clock() returns time since first call to time.clock
			self.timerOffset = time.time() - time.clock()
		else:
			self.time = time.time

		# Create a socket (SOCK_STREAM means a TCP socket)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# socket without naglingINFO
		self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
		# Connect to server and send data
		self.sock.settimeout(3)
		self.connect(host, port)

		# 'The first thing that a client should do after connecting to the RT Server is to
		# send the string “Version 1.0” to the server. This will ensure that the protocol
		# described in this document is followed by the server.'
		#self.sendCommand(b"Version 1.0") # FP does not need this, sled-server breaks on it
		# 'The second thing a client should do after connecting to the RT Server is to
		# change the byte order (unless the client uses Big Endian data, that is).'
		self.sendCommand(b"SetByteOrder BigEndian")

	def startStream(self, frequencyDivisor=1):
		"""Request server to start sending data"""
		self.sendCommand("StreamFrames FrequencyDivisor:{:d}".format(frequencyDivisor).encode("UTF-8"))

	def stopStream(self):
		"""Request server to stop sending data"""
		self.sendCommand(b"Bye")

	def close(self):
		"""Close socket"""
		print("closing Rtc3d socket")
		self.stopStream()
		self.sock.shutdown(socket.SHUT_RDWR)
		self.sock.close()

	# low level communication functions
	def connect(self, host, port):
		"""Connect to TCP socket """
		try:
			self.sock.connect((host, port))
		except Exception as e:
			logging.error("ERROR connecting to FP Server: "+str(e))
			raise

	def send(self, body, pType):
		"""Send package of type pType to server

		type  0: error, 1: command, 2: xml, 3: data, 4 nodata, 5: c3d
		pack length and type as 32 bit big endian."""
		head = struct.pack(">I", len(body)+8) + struct.pack(">I", pType)
		logging.debug("Sent: Body (%s): #%s#", len(body), body)
		try:
			self.sock.sendall(head+body)
		except Exception as e:
			logging.error("ERROR sending to FP server:\n%s", e)
			raise

	def sendHandshake(self):
		"""Send package of type *handshape* to server

		No mention of this package in the documentation. Does it appear in the
		stream of the example code?"""
		#head = struct.pack(">I", 0) + struct.pack("BBBB", 1, 3, 3, 6)
		pass

	def sendCommand(self, body):
		"""Send package of type *command* to server"""
		self.send(body, 1)

	def sendXml(self, body):
		"""Send package of type *xml* to server"""
		self.send(body, 2)

	def receive(self):
		""" wait for packet, save it in pSize, pType and pContent """
		try:
			# Receive data from the server and shut down
			# the size given is the buffer size, which is the maximum package size
			self.pSize = struct.unpack(">I", self.sock.recv(4))[0] # package size in bytes
			self.pType = struct.unpack(">I", self.sock.recv(4))[0]
			logging.debug("Received (%d, %s): ", self.pSize, self.pTypes[self.pType])
			if self.pSize > 8:
				self.pContent = self.sock.recv(self.pSize-8)
			else:
				self.pContent = b""
			return self.pType
		except Exception as e:
			logging.error("ERROR receiving from RTC3D server:\n%s", e)
			raise

	def time(self):
		"""on Windows, the highest resolution timer is time.clock()
		time.time() only has the resolution of the interrupt timer (1-15.6 ms)
		Note that time.clock does not provide time of day information.
		EXPECT DRIFT if you do not have ntp better than the MS version.
		This function is not used on other platforms."""
		return self.timerOffset + time.clock()

	def getBuffer(self):
		"""Return the buffered values."""
		return (copy.deepcopy(self.pContent), copy.deepcopy(self.pType))

	def parse(self):
		"""Parse pSize, pType and pContent

		Call appropriate function for parsing based on cType.
		"""
		if self.pType == 0: # error
			logging.error(self.pContent)
		elif self.pType == 1: # command
			logging.debug("command: %s", self.pContent)
		elif self.pType == 3: # data
			cCount = struct.unpack(">I", self.pContent[:4])[0] # component count
			iContent = 4 # byte index in content
			for _ in range(cCount):
				(cSize, cType, cFrame, cTime) = struct.unpack(">IIIQ", self.pContent[iContent:iContent+20])
				cTime *= 1e-6 # convert to second
				iContent += 20
				if cType == 1: # 3D
					values = self.parseComponent(self.pContent[iContent:iContent+cSize], 4)
					values *= 1e-3 # convert to meter
					self.handle3D(values[:, :3], cTime) # remove error column
				elif cType == 2: # Analog
					values = self.parseComponent(self.pContent[iContent:iContent+cSize], 1)
					self.handleAnalog(values, cTime)
				elif cType == 3: # Force
					values = self.parseComponent(self.pContent[iContent:iContent+cSize], 6)
					self.handleForce(values, cTime)
				elif cType == 4: # 6D
					values = self.parseComponent(self.pContent[iContent:iContent+cSize], 7)
					values[:, 4:3] *= 1e-3 # convert to meter
					self.handle6D(values[:, :7], cTime)  # remove error column
				iContent += cSize

	def parseComponent(self, cData, nFloat32):
		"""Parse 3D/Analog/Force/6D data components, return them as m x n array
		where m is the number of elements (markers, channels, plates, tools) and
		n is the number of 32 bit floats in an element.

		Args:
			cData (bytes): Component data.
			n (int): Number of 32 bit floats in element
		"""
		# number of elements
		[eCount] = struct.unpack(">I", cData[0:4])
		iContent = 4 # index in content
		eList = []
		for i in range(eCount):
			element = struct.unpack(">"+"f"*nFloat32, cData[iContent:iContent+4*nFloat32])
			iContent += 4*nFloat32
			eList.append(element)
		return np.matrix(eList, dtype=np.float32)

	def handle3D(self, m, t):
		"""Handle received 3D data components. Override this method in a subclass
		for asynchonous data handling.

		Args:
			m (n * 3 matrix of 32 bit float): Positions of all n markers
			t (float): Time according to host
		"""
		print("handle 3D: {:.3f}".format(t))

class Client(AsyncClient):
	"""Synchronous client for NDI First Principles
	"""
	def __init__(self, host='', port=3020):
		super().__init__(host, port)
		self.waitFor3D = threading.Event() # set after recieving 3D
		self.waitForAnalog = threading.Event()
		self.waitForForce = threading.Event()
		self.waitFor6D = threading.Event()
		self.receiverThread = threading.Thread(target=self.receiver)

		self.last3D = np.full((3, 1), np.nan, dtype=np.float)
		try:
			self.mainThread = threading.main_thread() # python 3
		except:
			self.mainThread = threading.current_thread() # python 2
		self.receiverThread.start()

		self.startStream()
		#self.wait3D() # wait for first value

	def receiver(self):
		"""background thread reading from the rtc3d stream """
		try:
			while self.mainThread.is_alive():
				self.receive()
				self.parse()
		finally:
			self.close()

	def handle3D(self, m, t):
		"""Update value of last3D and set the 3D event"""
		self.last3D = m
		self.waitFor3D.set() # tell anyone waiting for a value to stop waiting

	def wait3D(self, timeout=None):
		"""Wait for a new value from the rtc3d server and return it """
		self.waitFor3D.clear()
		self.waitFor3D.wait(timeout)
		return self.last3D

	def get3D(self):
		"""Return last received value """
		return self.last3D


try:
	import PyQt4.QtCore as QtCore
	class QClient(AsyncClient, QtCore.QThread):
		"""Emits the QSignals the3D theAnalog, theForce, the6D (names by NDI)
		"""
		the3D = QtCore.pyqtSignal(np.matrix)
		theAnalog = QtCore.pyqtSignal(np.matrix)
		theForce = QtCore.pyqtSignal(np.matrix)
		the6D = QtCore.pyqtSignal(np.matrix)
		def __init__(self, parent, host='', port=3020):
			AsyncClient.__init__(self, host, port)
			QtCore.QThread.__init__(self, parent)
			self.finished.connect(self.onTerminate)
			self.start()

		def handle3D(self, m, t):
			self.the3D.emit(m)

		def run(self):
			self.running = True
			self.paused = False
			self.startStream()
			while self.running:
				try:
					self.receive() # blocking
				except Exception:
					pass
				if not self.paused:
					self.parse()
			self.close()

		def pause(self):
			self.paused = True

		def unPause(self):
			self.paused = False
			
		def onTerminate(self):
			print("onTerminate")
except ImportError:
	class QClient(object):
		logging.error("PyQt was not installed")


